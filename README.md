# Advent of Code 2023

Solutions to Advent of Code 2023 using TypeScript and NodeJS 18

## Build and run.

1. Run 'npx tsc'

2. Run node build/main.js
