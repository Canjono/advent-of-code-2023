import * as fs from 'fs'

export default function day8(): void {
  solution1()
}

function solution1(): void {
  const lines = fs.readFileSync('src/day08/input.txt', 'utf-8').split('\n')

  const directions = lines[0].split('')
  const nodes = new Map<string, string[]>()

  lines.slice(2).forEach((line) => {
    const regex = /^(\w+) = \((\w+), (\w+)\)$/
    const matches = line.match(regex)

    nodes.set(matches[1], [matches[2], matches[3]])
  })

  let nodeValues = nodes.get('AAA')
  let steps = 0
  let dirIndex = 0

  while (true) {
    const nextNode = directions[dirIndex++] === 'L' ? nodeValues[0] : nodeValues[1]
    steps++

    if (nextNode === 'ZZZ') {
      break
    }

    nodeValues = nodes.get(nextNode)

    if (!nodeValues) {
      throw new Error('couldnt find node')
    }

    if (dirIndex >= directions.length) {
      dirIndex = 0
    }
  }

  console.log('steps: ', steps)
}
