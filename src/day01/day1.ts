import * as fs from 'fs'

export default function day1() {
  solution1()
  solution2()
}

function solution1() {
  const lines = fs.readFileSync('src/day01/input.txt', 'utf-8').split('\n')

  const regex = /\d{1}/g

  let calibrationValues = 0

  lines.forEach((line) => {
    const result = line.match(regex)

    const first = result[0]
    const second = result[result.length - 1]

    calibrationValues += +`${first}${second}`
  })

  console.log('values:', calibrationValues)
}

function solution2() {
  const lines = fs.readFileSync('src/day01/input.txt', 'utf-8').split('\n')

  const forwardRegex = /(one)|(two)|(three)|(four)|(five)|(six)|(seven)|(eight)|(nine)|(\d){1}/g
  const backRegex = /(eno)|(owt)|(eerht)|(ruof)|(evif)|(xis)|(neves)|(thgie)|(enin)|(\d){1}/g

  let calibrationValues = 0

  lines.forEach((line) => {
    const forwardResult = line.match(forwardRegex)
    const backResult = line.split('').reverse().join('').match(backRegex)

    const first = convertToNumber(forwardResult[0])
    const second = convertToNumber(backResult[0])

    calibrationValues += +`${first}${second}`
  })

  console.log('result:', calibrationValues)
}

function convertToNumber(num: string): number {
  switch (num) {
    case 'one':
    case 'eno':
      return 1
    case 'two':
    case 'owt':
      return 2
    case 'three':
    case 'eerht':
      return 3
    case 'four':
    case 'ruof':
      return 4
    case 'five':
    case 'evif':
      return 5
    case 'six':
    case 'xis':
      return 6
    case 'seven':
    case 'neves':
      return 7
    case 'eight':
    case 'thgie':
      return 8
    case 'nine':
    case 'enin':
      return 9
    default:
      return +num
  }
}
