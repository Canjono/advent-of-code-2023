import * as fs from 'fs'

const ranks = new Map<string, number>()
ranks.set('A', 14)
ranks.set('K', 13)
ranks.set('Q', 12)
ranks.set('J', 11)
ranks.set('T', 10)
ranks.set('9', 9)
ranks.set('8', 8)
ranks.set('7', 7)
ranks.set('6', 6)
ranks.set('5', 5)
ranks.set('4', 4)
ranks.set('3', 3)
ranks.set('2', 2)

export default function day7(): void {
  solution1()
  solution2()
}

function solution1(useJoker: boolean = false): void {
  const lines = fs.readFileSync('src/day07/input.txt', 'utf-8').split('\n')

  const hands = lines.map((line) => {
    const hand = line.split(' ')

    return <Hand>{
      cards: hand[0],
      bid: +hand[1],
      type: getType(hand[0], useJoker),
    }
  })

  sortHands(hands)

  let result = 0

  for (let i = 0; i < hands.length; i++) {
    result += hands[i].bid * (i + 1)
  }

  console.log(result)
}

function solution2(): void {
  ranks.set('J', 1)

  solution1(true)
}

function getType(cards: string, useJoker: boolean = false): Type {
  const sameCards = new Map<string, number>()

  for (let i = 0; i < cards.length; i++) {
    const card = cards[i]

    if (sameCards.has(card)) {
      sameCards.set(card, sameCards.get(card) + 1)
    } else {
      sameCards.set(card, 1)
    }
  }

  let jokerAmount = useJoker ? sameCards.get('J') : null

  if (sameCards.size === 5) {
    if (jokerAmount) {
      return Type.OnePair
    }
    return Type.HighCard
  }

  if (sameCards.size === 4) {
    if (jokerAmount) {
      return Type.ThreeOfAKind
    }
    return Type.OnePair
  }

  if (sameCards.size === 1) {
    return Type.FiveOfAKind
  }

  if (sameCards.size === 2 && jokerAmount) {
    return Type.FiveOfAKind
  }

  const amounts: number[] = []

  for (let [card, amount] of sameCards) {
    if (amount === 4) {
      return Type.FourOfAKind
    }

    amounts.push(amount)
  }

  if (amounts.includes(3)) {
    if (jokerAmount === 2) {
      return Type.FiveOfAKind
    }

    if (jokerAmount === 1) {
      return Type.FourOfAKind
    }

    if (jokerAmount === 3) {
      return amounts.includes(2) ? Type.FiveOfAKind : Type.FourOfAKind
    }
    return amounts.includes(2) ? Type.FullHouse : Type.ThreeOfAKind
  }

  if (jokerAmount === 1) {
    return Type.FullHouse
  }

  if (jokerAmount === 2) {
    return Type.FourOfAKind
  }

  return Type.TwoPair
}

function sortHands(hands: Hand[]): void {
  hands.sort((a, b) => {
    if (a.type > b.type) {
      return 1
    }

    if (a.type < b.type) {
      return -1
    }

    for (let i = 0; i < a.cards.length; i++) {
      const aRank = ranks.get(a.cards[i])
      const bRank = ranks.get(b.cards[i])

      if (aRank > bRank) {
        return 1
      }

      if (aRank < bRank) {
        return -1
      }
    }

    return 0
  })
}

interface Hand {
  cards: string
  type: Type
  bid: number
}

enum Type {
  HighCard,
  OnePair,
  TwoPair,
  ThreeOfAKind,
  FullHouse,
  FourOfAKind,
  FiveOfAKind,
}
