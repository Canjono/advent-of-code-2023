import * as fs from 'fs'

export default function day3() {
  solution1()
  solution2()
}

function solution1() {
  const lines = getLines()
  const engineParts = getEngineParts(lines)

  const partNumbers = engineParts.filter((part) => {
    return part.hasAdjacentSymbol(lines)
  })

  const sum = partNumbers.reduce((acc, curr) => {
    return acc + curr.num
  }, 0)

  console.log(sum)
}

function solution2() {
  const lines = getLines()
  const engineParts = getEngineParts(lines)

  const asterixSymbols = new Map<string, EnginePart[]>()

  engineParts.forEach((part) => {
    const adjacentAsterixes = part.getAdjacentAsterixSymbols(lines)

    adjacentAsterixes.forEach((asterix) => {
      const key = `${asterix.x}-${asterix.y}`
      const parts = asterixSymbols.has(key) ? [...asterixSymbols.get(key), part] : [part]
      asterixSymbols.set(`${asterix.x}-${asterix.y}`, parts)
    })
  })

  const gearRatios: number[] = []

  asterixSymbols.forEach((x) => {
    if (x.length === 2) {
      gearRatios.push(x[0].num * x[1].num)
    }
  })

  const sum = gearRatios.reduce((acc, curr) => {
    return acc + curr
  }, 0)

  console.log(sum)
}

function getLines(): string[] {
  return fs.readFileSync('src/day03/input.txt', 'utf-8').split('\n')
}

function getEngineParts(lines: string[]): EnginePart[] {
  const parts: EnginePart[] = []

  lines.forEach((line, y) => {
    const regex = /\d+/g

    let matches = regex.exec(line)

    while (matches) {
      const part = new EnginePart(+matches[0], y, matches.index)
      parts.push(part)

      matches = regex.exec(line)
    }
  })

  return parts
}

class EnginePart {
  num: number
  startX: number
  y: number

  get endX(): number {
    return this.startX + this.num.toString().length - 1
  }

  constructor(num: number, y: number, startX: number) {
    this.num = num
    this.y = y
    this.startX = startX
  }

  getAdjacentChars(lines: string[]): Char[] {
    const endX = this.endX

    const adjacentChars = [
      new Char(lines[this.y][this.startX - 1], this.y, this.startX - 1), // left side.
      new Char(lines[this.y][this.endX + 1], this.y, this.endX + 1), // right side.
    ]

    for (let i = this.startX - 1; i <= endX + 1; i++) {
      // Above.
      if (this.y > 0) {
        adjacentChars.push(new Char(lines[this.y - 1][i], this.y - 1, i))
      }

      // Below.
      if (this.y < lines.length - 1) {
        adjacentChars.push(new Char(lines[this.y + 1][i], this.y + 1, i))
      }
    }

    return adjacentChars
  }

  hasAdjacentSymbol(lines: string[]): boolean {
    const adjacentChars = this.getAdjacentChars(lines)

    return adjacentChars.some((char) => char.isSymbol())
  }

  getAdjacentAsterixSymbols(lines: string[]): Char[] {
    const adjacentChars = this.getAdjacentChars(lines)

    return adjacentChars.filter((char) => char.hasAsterixSymbol())
  }

  isSymbol(char: string): boolean {
    return char && char !== '.' && !Number.isInteger(char)
  }
}

class Char {
  char: string
  y: number
  x: number

  constructor(char: string, y: number, x: number) {
    this.char = char
    this.y = y
    this.x = x
  }

  isSymbol(): boolean {
    return this.char && this.char !== '.' && !Number.isInteger(this.char)
  }

  hasAsterixSymbol(): boolean {
    return this.char === '*'
  }
}
