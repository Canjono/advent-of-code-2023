import * as fs from 'fs'

export default function day2(): void {
  solution1()
  solution2()
}

function solution1(): void {
  const games = getGames()

  const result = games.reduce((acc, curr) => {
    return curr.isPossible ? (acc += curr.id) : acc
  }, 0)

  console.log(result)
}

function solution2(): void {
  const games = getGames()

  const result = games.reduce((acc, curr) => {
    return (acc += curr.fewestPow)
  }, 0)

  console.log(result)
}

function getGames(): Game[] {
  const lines = fs.readFileSync('src/day02/input.txt', 'utf-8').split('\n')

  return lines.map((line) => {
    const regex = /^Game (\d+): (.+)$/g
    const result = regex.exec(line)

    const game = new Game(+result[1])

    const cubeRegex = /(\d+ blue)|(\d+ red)|(\d+ green)/g
    const cubeResult = result[2].match(cubeRegex)

    cubeResult.forEach((x) => {
      const splitted = x.split(' ')
      const amount = +splitted[0]
      const color = splitted[1]

      switch (color) {
        case 'blue':
          if (game.maxBlue < amount) {
            game.maxBlue = amount
          }
          break
        case 'green':
          if (game.maxGreen < amount) {
            game.maxGreen = amount
          }
          break
        case 'red':
          if (game.maxRed < amount) {
            game.maxRed = amount
          }
          break
        default:
          throw new Error('Something went wrong!!!')
      }
    })

    return game
  })
}

class Game {
  id: number
  maxGreen = 0
  maxBlue = 0
  maxRed = 0

  get isPossible(): boolean {
    return this.maxRed <= 12 && this.maxGreen <= 13 && this.maxBlue <= 14
  }

  get fewestPow(): number {
    return this.maxRed * this.maxGreen * this.maxBlue
  }

  constructor(id: number) {
    this.id = id
  }
}
