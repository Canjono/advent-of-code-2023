import * as fs from 'fs'

export default function day5() {
  solution1()
  solution2()
}

function solution1(): void {
  const almanac = getAlmanac()
  const lowestLocation = getLowestLocation(almanac.seeds, almanac)

  console.log(lowestLocation)
}

function solution2() {
  // NOT COMPLETE!
  const almanac = getAlmanac()

  let lowestLocation = Number.MAX_VALUE

  for (let i = 0; i < almanac.seeds.length; i += 2) {
    const seeds: number[] = []
    const startSeed = almanac.seeds[i]
    const endSeed = startSeed + almanac.seeds[i + 1]

    for (let j = startSeed; j < endSeed; j++) {
      lowestLocation = getLowestLocation([j], almanac)
    }
  }

  console.log(lowestLocation)
}

function getAlmanac(): Almanac {
  const lines = fs.readFileSync('src/day05/input.txt', 'utf-8').split('\n')

  const seeds = lines[0]
    .split(':')[1]
    .split(' ')
    .filter((x) => x)
    .map((x) => +x)

  const almanac = new Almanac(seeds)

  let currMap

  for (let i = 2; i < lines.length; i++) {
    const line = lines[i]

    if (!line) {
      almanac.maps.push(currMap)
      continue
    }

    if (line.includes('map')) {
      const parts = line.split(' ')[0].split('-')

      currMap = new ConvertMap(<Category>parts[0], <Category>parts[2])
      continue
    }

    const conversion = line.split(' ').map((x) => +x)

    currMap.conversions.push([...conversion])
  }

  almanac.maps.push(currMap)

  return almanac
}

function getLowestLocation(seeds: number[], almanac: Almanac): number {
  let lowestLocation = Number.MAX_VALUE

  seeds.forEach((seed) => {
    let currNumber = seed

    for (let i = 0; i < almanac.maps.length; i++) {
      const foundConversion = almanac.maps[i].conversions.find((conversion) => {
        const destination = conversion[0]
        const source = conversion[1]
        const range = conversion[2]

        return currNumber >= source && currNumber < source + range
      })

      currNumber = foundConversion ? foundConversion[0] + (currNumber - foundConversion[1]) : currNumber
    }

    if (currNumber < lowestLocation) {
      lowestLocation = currNumber
    }
  })

  return lowestLocation
}

class Almanac {
  seeds: number[]
  maps: ConvertMap[] = []

  constructor(seeds: number[]) {
    this.seeds = seeds
  }
}

class ConvertMap {
  source: Category
  destination: Category
  conversions: number[][] = []

  constructor(source: Category, destination: Category) {
    this.source = source
    this.destination = destination
  }
}

type Category = 'seed' | 'soil' | 'fertilizer' | 'water' | 'light' | 'temperature' | 'humidity' | 'location'
