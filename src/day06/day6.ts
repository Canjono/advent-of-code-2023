import * as fs from 'fs'

export default function day6(): void {
  solution1()
  solution2()
}

function solution1(): void {
  const lines = getLines()

  const times = lines[0]
    .split(':')[1]
    .split(' ')
    .filter((x) => x)
    .map((x) => +x)

  const distances = lines[1]
    .split(':')[1]
    .split(' ')
    .filter((x) => x)
    .map((x) => +x)

  const numberOfWays: number[] = []

  for (let i = 0; i < times.length; i++) {
    const time = times[i]
    const distance = distances[i]

    numberOfWays.push(getNumberOfWays(time, distance))
  }

  const result = numberOfWays.reduce((acc, curr) => {
    return acc * curr
  }, 1)

  console.log(result)
}

function solution2(): void {
  const lines = getLines()

  const time = parseInt(lines[0].split(':')[1].replace(/\s/g, ''))
  const distance = parseInt(lines[1].split(':')[1].replace(/\s/g, ''))

  const numberOfWays = getNumberOfWays(time, distance)

  console.log(numberOfWays)
}

function getLines(): string[] {
  return fs.readFileSync('src/day06/input.txt', 'utf-8').split('\n')
}

function getNumberOfWays(time: number, distance: number): number {
  let amount = 0

  for (let i = 1; i < time; i++) {
    if (i * (time - i) > distance) {
      amount++
    }
  }

  return amount
}
