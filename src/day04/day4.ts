import * as fs from 'fs'

export default function day4() {
  solution1()
  solution2()
}

function solution1(): void {
  const cards = getOriginalCards()

  const points = cards.map((card) => card.getPoints())

  const sum = points.reduce((acc, curr) => {
    return acc + curr
  }, 0)

  console.log(sum)
}

function solution2(): void {
  const cards = getOriginalCards()

  const cardsMap = new Map<number, Card>()

  cards.forEach((card) => {
    cardsMap.set(card.id, card)
  })

  const totalCount = cards.reduce((acc, curr) => {
    acc++
    const amount = getAmountOfCopies(curr, cardsMap)

    return (acc += amount)
  }, 0)

  console.log(totalCount)
}

function getAmountOfCopies(card: Card, allCards: Map<number, Card>): number {
  const copies = card.getCopies(allCards)

  if (copies.length === 0) {
    return 0
  }

  let amount = copies.length

  for (let i = 0; i < copies.length; i++) {
    amount += getAmountOfCopies(copies[i], allCards)
  }

  return amount
}

function getOriginalCards(): Card[] {
  const lines = fs.readFileSync('src/day04/input.txt', 'utf-8').split('\n')

  return lines.map((line) => {
    const idAndNumbers = line.split(':')
    const id = idAndNumbers[0].split(' ').filter((x) => x)[1]
    const numbers = idAndNumbers[1].split('|')

    const winningNumbers = numbers[0]
      .trim()
      .split(' ')
      .filter((x) => x) // Remove empty results.
      .map((num) => +num)

    const yourNumbers = numbers[1]
      .trim()
      .split(' ')
      .filter((x) => x) // Remove empty results.
      .map((num) => +num)

    return new Card(+id, winningNumbers, yourNumbers)
  })
}

class Card {
  id: number
  winningNumbers: number[] = []
  yourNumbers: number[] = []

  constructor(id: number, winningNumbers: number[], yourNumbers: number[]) {
    this.id = id
    this.winningNumbers = winningNumbers
    this.yourNumbers = yourNumbers
  }

  amountOfWins(): number {
    return this.yourNumbers.filter((num) => this.winningNumbers.includes(num)).length
  }

  getPoints(): number {
    const amountOfWins = this.amountOfWins()

    if (amountOfWins === 0) {
      return 0
    }

    let points = 1

    for (let i = 1; i < amountOfWins; i++) {
      points *= 2
    }

    return points
  }

  getCopies(allCards: Map<number, Card>): Card[] {
    const amountOfWins = this.amountOfWins()

    const copies: Card[] = []

    for (let i = 1; i <= amountOfWins; i++) {
      const cardToCopy = allCards.get(this.id + i)

      if (!cardToCopy) {
        break
      }

      copies.push(new Card(cardToCopy.id, cardToCopy.winningNumbers, cardToCopy.yourNumbers))
    }

    return copies
  }
}
